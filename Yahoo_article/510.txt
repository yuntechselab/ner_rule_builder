由於出口持續疲弱，國內活動又因政治動盪受到影響，泰國今年第一季經濟萎縮幅度高於預期，可能陷入衰退風險。 曼谷全國經濟及社會發展委員會（National Economic ＆ Social Development Board）今天公佈，泰國第1季國內生產毛額（GDP）較前季衰退2.1%。前季經濟修正後擴帳0.1%。 全國經濟及社會發展委員會也下修今年成長率為1.5%至2.5%，遠低於先前預期的3%到4%。中央社（翻譯）。