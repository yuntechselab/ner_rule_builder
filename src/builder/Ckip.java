package builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Ckip {

	private static final String ckipurl = "http://sunlight.iis.sinica.edu.tw/cgi-bin/text.cgi"; // CKIP嚙踝蕭��蕭嚙質嚙�

//	public static void main(String args[]) throws MalformedURLException, IOException{
//		System.out.println(originalCKIIPStr("大頭"));
//	}
	public static String getCkipResult(String news) throws IOException {

		// System.out.println(originalCKIIPStr(news));

		return originalCKIIPStr(news);

	}

	public static String originalCKIIPStr(String text)
			throws MalformedURLException, IOException {

		String originalCKIPStr = getCKIPOriginalString(text);

		Document doc = Jsoup.parse(new URL(originalCKIPStr).openStream(),
				"Big5", originalCKIPStr);

		String str = doc.select("body").text();

		return String.valueOf(str);
	}

	private static String getPostResponse(String link,
			HashMap<String, String> data) { // 嚙踐撒�Post���嚙踐□嚙踐◢esponse
		// TODO Auto-generated method stub
		StringBuffer buf = new StringBuffer();

		try {
			URL url = new URL(link);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Referer",
					"http://sunlight.iis.sinica.edu.tw/uwextract/");
			conn.setRequestProperty("Content-Type", "text;charset=big5");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			PrintWriter output = new PrintWriter(new OutputStreamWriter(
					conn.getOutputStream(), "CP950"));

			for (String param : data.keySet()) {
				output.print(param + "=" + data.get(param));
				// System.out.println(param+"="+data.get(param));
				output.flush();
			}

			output.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "CP950"));
			String line;

			while ((line = in.readLine()) != null) {
				buf.append(line + "\n");
			}
			// System.out.println(conn.getInputStream()+"\n"+buf);
			in.close();
			conn.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return buf.toString();
	}

	private static String getCKIPOriginalString(String segmentStr) {// CKIPOriginalString
																	// =
																	// 嚙踝蕭���蕭謓荔蕭貔豯佗蕭���豯伐蕭謕蕭謢賂蕭��蕭嚙�

		HashMap<String, String> data = new HashMap<String, String>();
		data.put("query", segmentStr); // key 嚙踐�textbox嚙踝蕭��蕭��蕭��蕭����
		String ckipout = getPostResponse(ckipurl, data);

		String id = ckipout.substring(ckipout.indexOf("pool/") + 5,
				ckipout.lastIndexOf(".html"));
		// System.out.println(id);
		String ans1 = "http://sunlight.iis.sinica.edu.tw/uwextract/pool/" + id
				+ ".txt.process.txt";
		String ans2 = "http://sunlight.iis.sinica.edu.tw/uwextract/pool/" + id
				+ ".tag.txt";
		String ans3 = "http://sunlight.iis.sinica.edu.tw/uwextract/pool/" + id
				+ ".uw.txt";

		return ans2;
	}
}
