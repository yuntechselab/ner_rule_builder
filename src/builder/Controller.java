package builder;

import java.io.IOException;
import java.sql.SQLException;

import ner_rule.Model;
import ner_rule.View;

public class Controller {
	private Model model;
	private View view;

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
	}
	
	 public Controller() {
		// TODO Auto-generated constructor stub
	}

	private EntityBuilder entityBuilder;

	  public void setEntityBuilder(EntityBuilder eb) { entityBuilder = eb; }
	  public Article getArticle() { return entityBuilder.getArticle(); }

	  public void constructArticle(String tableName, Integer id) throws IOException, SQLException {
		  entityBuilder.createNewArticle();
		  entityBuilder.importArticle(tableName, id);
		  entityBuilder.ckipMethod(tableName, id);
		  entityBuilder.segement();
		  entityBuilder.personKeepAllPos(tableName, id);
		  entityBuilder.placeKeepAllPos(tableName, id);
		  entityBuilder.personKeepSomePos(tableName, id);
		  entityBuilder.placeKeepSomePos(tableName, id);

	  }
}