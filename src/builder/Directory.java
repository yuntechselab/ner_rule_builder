package builder;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Directory {
	ArrayList<String> fileNameList = new ArrayList<String>();
	ArrayList<Object> fileArrayList = new ArrayList<Object>();

	String directoryName = "";

	public Directory(String directoryName) {
		this.directoryName = directoryName;
	}

	public ArrayList<String> getDirectoryAllFileName() {
		File file = new File(directoryName);
		if (file.isDirectory()) {
			// System.out.println("filename : " + file.getName());
			String[] s = file.list(); // 把資料夾裡面的檔案名稱放進s
			// System.out.println("size : " + s.length); // s的大小就是有多少檔案

			for (int i = 0; i < s.length; i++) {
				// System.out.println(s[i]);
				fileNameList.add(s[i]); // 檔案名稱放進file name list
			}
		}
		return fileNameList;
	}

}
