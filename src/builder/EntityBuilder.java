package builder;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import rule.PersonRule;
import rule.PlaceRule;
import rule.Rule;

public abstract class EntityBuilder {
	protected Article article;

	public Article getArticle() {
		return article;
	}

	public void createNewArticle() {
		article = new Article();
	}

	public void ckipMethod(String tableName, Integer id) throws IOException,
			SQLException {
		// article.getCkipResult();
		Sql db = new Sql();
		Connection ckipCon = db.getConnection();
		String sql = "select ckip from " + tableName + " where id = '" + id
				+ "'";
		String ckipResult = db.getString(ckipCon, sql, "ckip");
		ckipCon.close();
		if (ckipResult != null && !ckipResult.equals("")) {
			System.out.println("yes ckip file");
			article.setCkipResult(ckipResult);
		} else {
			System.out.println("no ckip file");
			article.setCkipResult(Ckip.originalCKIIPStr(article.getContent()));
			sql = "update ckipTable set ckip ='" + article.getCkipResult()
					+ "' where id =" + id;
			Connection updateCon = db.getConnection();
			db.updateString(updateCon, sql);
			updateCon.close();
		}
	}

	public void segement() {
		article.segement();
	};

	public abstract void keepPos();

	public void buildDough() {
		// TODO Auto-generated method stub

	}

	public void buildSauce() {
		// TODO Auto-generated method stub

	}

	public void buildTopping() {
		// TODO Auto-generated method stub

	}

	public void importArticle(String tableName, Integer id) throws IOException,
			SQLException {
		// TODO Auto-generated method stub
		article.setFileName(id);

		Sql db = new Sql();
		Connection con = db.getConnection();
		String sql = "select article from " + tableName + " where id = " + id;
		// ResultSet rs = sqlite.selectAll(sql);
		article.setContent(db.getString(con, sql, "article"));

		System.out.println("Content(article get): " + article.getContent());
		con.close();
	}

	public void personKeepAllPos(String tableName, Integer id)
			throws IOException, SQLException {
		// TODO Auto-generated method stub
		// article.setAllPosPersonResult(article.executeNerStrategy(new
		// PersonRule()));
		// article.executeNerStrategy(new PersonRule());
		Rule personRule = new PersonRule();
		// System.out.println(article.ckipResult);
		// article.setAllPosPersonResult(personRule.nameEntityRecognize(article));

		Sql db = new Sql();
		Connection con = db.getConnection();
		String sql = "select questionPersonAllPos from " + tableName
				+ " where id = '" + id + "'";
		ArrayList<String> result = new ArrayList<String>();
		try {
			result = db.getArrayList(con, sql, "questionPersonAllPos");
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result.get(0) != null && !result.equals("")) {
			System.out.println("yes allPos file");
			article.setAllPosPerson(result);
			System.out.println("all pos result: " + result);
		} else {
			System.out.println("no allPos file");
			sql = "update " + tableName + " set questionPersonAllPos ='"
					+ personRule.nameEntityRecognize(article) + "' where id ="
					+ id;
			Connection updateCon = db.getConnection();
			db.updateString(updateCon, sql);
			updateCon.close();
		}
	}

	public void personKeepSomePos(String tableName, Integer id)
			throws IOException, SQLException {
		// TODO Auto-generated method stub
		// article.setAllPosPersonResult(article.executeNerStrategy(new
		// PersonRule()));
		// article.executeNerStrategy(new PersonRule());
		Rule personRule = new PersonRule();
		// System.out.println(article.ckipResult);
		// article.setAllPosPersonResult(personRule.nameEntityRecognize(article));

		Sql db = new Sql();
		Connection con = db.getConnection();
		String sql = "select questionPersonSomePos from " + tableName
				+ " where id = '" + id + "'";
		ArrayList<String> result = new ArrayList<String>();
		try {
			result = db.getArrayList(con, sql, "questionPersonSomePos");
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result.get(0) != null && !result.equals("")) {
			System.out.println("yes somePos file");
			article.setAllPosPerson(result);
			System.out.println("some pos result: " + result);
		} else {
			System.out.println("no somePos file");
			article.keepPos();
			sql = "update " + tableName + " set questionPersonSomePos ='"
					+ personRule.nameEntityRecognize(article) + "' where id ="
					+ id;
			Connection updateCon = db.getConnection();
			db.updateString(updateCon, sql);
			updateCon.close();
		}
	}

	public void placeKeepAllPos(String tableName, Integer id)
			throws IOException, SQLException {
		// TODO Auto-generated method stub
		// article.setAllPosPersonResult(article.executeNerStrategy(new
		// PersonRule()));
		// article.executeNerStrategy(new PersonRule());
		Rule placeRule = new PlaceRule();
		// System.out.println(article.ckipResult);
		// article.setAllPosPersonResult(personRule.nameEntityRecognize(article));

		Sql db = new Sql();
		Connection con = db.getConnection();
		String sql = "select questionPlaceAllPos from " + tableName
				+ " where id = '" + id + "'";
		ArrayList<String> result = new ArrayList<String>();
		try {
			result = db.getArrayList(con, sql, "questionPlaceAllPos");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			con.close();
		}
		if (result.get(0) != null && !result.equals("")) {
			System.out.println("yes allPos place file");
			article.setAllPosPerson(result);
			System.out.println("all pos place result: " + result);
		} else {
			System.out.println("no allPos place file");
			sql = "update " + tableName + " set questionPlaceAllPos ='"
					+ placeRule.nameEntityRecognize(article) + "' where id ="
					+ id;
			System.out.println("allpos place: "
					+ placeRule.nameEntityRecognize(article));
			Connection updateCon = db.getConnection();
			db.updateString(updateCon, sql);
			updateCon.close();
		}
	}
	public void placeKeepSomePos(String tableName, Integer id)
			throws IOException, SQLException {
		// TODO Auto-generated method stub
		// article.setAllPosPersonResult(article.executeNerStrategy(new
		// PersonRule()));
		// article.executeNerStrategy(new PersonRule());
		Rule placeRule = new PlaceRule();
		// System.out.println(article.ckipResult);
		// article.setAllPosPersonResult(personRule.nameEntityRecognize(article));

		Sql db = new Sql();
		Connection con = db.getConnection();
		String sql = "select questionPlaceSomePos from " + tableName
				+ " where id = '" + id + "'";
		ArrayList<String> result = new ArrayList<String>();
		try {
			result = db.getArrayList(con, sql, "questionPlaceSomePos");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			con.close();
		}
		if (result.get(0) != null && !result.equals("")) {
			System.out.println("yes somePos place file");
			article.setAllPosPerson(result);
			System.out.println("some pos place result: " + result);
		} else {
			System.out.println("some allPos place file");
			article.keepPos();
			sql = "update " + tableName + " set questionPlaceSomePos ='"
					+ placeRule.nameEntityRecognize(article) + "' where id ="
					+ id;
			System.out.println("some pos place: "
					+ placeRule.nameEntityRecognize(article));
			Connection updateCon = db.getConnection();
			db.updateString(updateCon, sql);
			updateCon.close();
		}
	}
}