package builder;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import ner_rule.Directory;

public class MvcDemo {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Model model = new Model();
		View view = new View();
		// Controller controller = new Controller(model, view);
		String tableName = "ckipTable";
		String articleDirectory = "MET2_article_utf8_音節號";

		String answerDirectoryNoOneWordName = "";
		String answerDirectoryYesOneWordName = "";

		String allPosQuestionDirectory = "Yahoo_person_question_allPos";
		String somePosQuestionDirectory = "Yahoo_person_question_somePos";

		String missAndWrong = "Yahoo_missAndWrong";
		String resultDirectory = "Yahoo_result";

		Controller controller = new Controller();
		EntityBuilder personEntityBuilder = new PersonEntityBuilder();

		controller.setEntityBuilder(personEntityBuilder);
		
		ArrayList<Integer> idList = new ArrayList<Integer>();
		String sql = null;
		sql = "select * from " + tableName;
		Sql db = new Sql();
		Connection con = db.getConnection();
		
		idList = db.getId(con, sql);
		con.close();
		for (Integer id : idList) {
			System.out.println("id: " + id);
			controller.constructArticle(tableName, id);
			Article article = controller.getArticle();
			System.out.println("all pos person result: " + article.getAllPosPersonResult());
//			System.exit(0);
		}
	}
}