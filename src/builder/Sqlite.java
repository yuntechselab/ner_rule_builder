package builder;

import java.io.Closeable;
import java.sql.*;
import java.util.ArrayList;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;
import org.sqlite.core.DB;

public class Sqlite {
	private Connection connect() {
		// SQLite connection string
		String url = "jdbc:sqlite:hello.db";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	public void selectAll() {
		String sql = "SELECT id, fileName FROM ckipTable";

		try (Connection conn = this.connect();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getInt("id") + "\t"
						+ rs.getString("fileName") + "\t");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	// public Sqlite() {
	// try {
	// SQLiteConfig config = new SQLiteConfig();
	// // config.setReadOnly(true);
	// config.setSharedCache(true);
	// config.enableRecursiveTriggers(true);
	//
	// SQLiteDataSource ds = new SQLiteDataSource(config);
	// ds.setUrl("jdbc:sqlite:hello.db");
	// conn = ds.getConnection();
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	// public Connection getConnection() throws SQLException {
	// SQLiteConfig config = new SQLiteConfig();
	// // config.setReadOnly(true);
	// config.setSharedCache(true);
	// config.enableRecursiveTriggers(true);
	//
	// SQLiteDataSource ds = new SQLiteDataSource(config);
	// ds.setUrl("jdbc:sqlite:hello.db");
	// return ds.getConnection();
	// // ds.setServerName("sample.db");
	//
	// }

	// public Connection Sqlite() throws SQLException {
	// SQLiteConfig config = new SQLiteConfig();
	// // config.setReadOnly(true);
	// config.setSharedCache(true);
	// config.enableRecursiveTriggers(true);
	//
	// SQLiteDataSource ds = new SQLiteDataSource(config);
	// ds.setUrl("jdbc:sqlite:hello.db");
	// return ds.getConnection();
	// // ds.setServerName("sample.db");
	//
	// }

	// create Table
	public void createTable(Connection con) throws SQLException {
		String sql = "DROP TABLE IF EXISTS ckipTable ;create table ckipTable (id integer primary key, name char(10)); ";
		Statement stat = null;
		stat = con.createStatement();
		stat.executeUpdate(sql);
	}

	// drop table
	public void dropTable(Connection con) throws SQLException {
		String sql = "drop table test ";
		Statement stat = null;
		stat = con.createStatement();
		stat.executeUpdate(sql);
	}

	// 新增
	public void insert(Connection con, int id, String name) throws SQLException {
		String sql = "insert into test (id,name) values(?,?)";
		PreparedStatement pst = null;
		pst = con.prepareStatement(sql);
		int idx = 1;
		pst.setInt(idx++, id);
		pst.setString(idx++, name);
		pst.executeUpdate();
	}

	public void insertSql(Connection con, String sql) throws SQLException {
		// sql = "insert into test (id,name) values(?,?)";
		// Connection conn = this.connect();
		try {
			PreparedStatement pst = null;
			pst = con.prepareStatement(sql);
			pst.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}
	}

	public void insertCkip(Connection con, int id, String fileName, String ckip)
			throws SQLException {
		String sql = "insert into ckipTable (id,fileName,ckip) values(?,?,?)";
		PreparedStatement pst = null;
		pst = con.prepareStatement(sql);
		int idx = 1;
		pst.setInt(idx++, id);
		pst.setString(idx++, fileName);
		pst.setString(idx++, ckip);
		pst.executeUpdate();
	}

	// 修改
	public void update(Connection con, int id, String name) throws SQLException {
		String sql = "update test set name = '123' where id = 1";
		PreparedStatement pst = null;
		pst = con.prepareStatement(sql);
		// int idx = 1;

		pst.executeUpdate();
	}

	// 刪除
	public void delete(Connection con, int id) throws SQLException {
		String sql = "delete from test where id = ?";
		PreparedStatement pst = null;
		pst = con.prepareStatement(sql);
		int idx = 1;
		pst.setInt(idx++, id);
		pst.executeUpdate();
	}

	public void selectAll(Connection con) throws SQLException {
		String sql = "select * from CkipTable";
		Statement stat = null;
		ResultSet rs = null;
		stat = con.createStatement();
		rs = stat.executeQuery(sql);
		while (rs.next()) {
			System.out.println(rs.getInt("id") + "\t"
					+ rs.getString("fileName"));
		}
	}

	public ResultSet selectAll(String sql) throws SQLException {
		ResultSet resultSet = null;
		try {
			Connection conn = this.connect();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// loop through the result set
			// while (rs.next()) {
			// System.out.println(rs.getInt("id") + "\t"
			// + rs.getString("fileName") + "\t");
			// }
			resultSet = rs;
			// System.out.println(conn.isClosed());

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultSet;

	}

	public ArrayList selectFileName(String sql) throws SQLException {
		ArrayList<String> arrayList = new ArrayList();
		try {
			Connection conn = this.connect();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// loop through the result set
			while (rs.next()) {
				arrayList.add(rs.getString("fileName"));
				System.out.println(rs.getInt("id") + "\t"
						+ rs.getString("fileName") + "\t");
			}
			conn.close();
			stmt.close();
			System.out.println("conn state: " + conn.isClosed());

			return arrayList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return arrayList;

	}

	// public String getSqliteCkip(Connection con, String fileName)
	// throws SQLException {
	// String sql = "select * from CkipTable";
	// Statement stat = null;
	// ResultSet rs = null;
	// stat = con.createStatement();
	// rs = stat.executeQuery(sql);
	// while (rs.next()) {
	// // System.out.println(rs.getInt("id") + "\t" +
	// // rs.getString("ckip"));
	// if (rs.getString("fileName").equals(fileName))
	// break;
	// }
	// return rs.getString("ckip");
	// }

	public String getArticle(String sql) throws SQLException {
		String result = null;
		try {
			Connection conn = this.connect();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// loop through the result set
			while (rs.next()) {
				result = rs.getString("article");
				System.out.println(rs.getInt("id") + "\t"
						+ rs.getString("fileName") + "\t"
						+ rs.getString("article") + "\t");
			}
			conn.close();
			stmt.close();
			rs.close();
			System.out.println("conn state: " + conn.isClosed());

			return result;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return result;

	}
	public String getCkip(String sql) throws SQLException {
		String result = null;
		try {
			Connection conn = this.connect();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// loop through the result set
			while (rs.next()) {
				result = rs.getString("ckip");
				System.out.println(rs.getInt("id") + "\t"
						+ rs.getString("fileName") + "\t"
						+ rs.getString("ckip") + "\t");
			}
			conn.close();
			stmt.close();
			rs.close();
			System.out.println("conn state: " + conn.isClosed());

			return result;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return result;

	}

	public Boolean isExist(String tableName, String columnName,
			String fileName, String sql) throws SQLException {

		try {
			Connection conn = this.connect();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getString(columnName));
				if (rs.getString(columnName) != null) {
					return true;
				}
			}
			conn.close();
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return false;

	}
//
//	public static void main(String args[]) throws SQLException {
		// Sqlite test = new Sqlite();
		// Connection con = test.getConnection();
		// 建立table
		// test.createTable(con);
		// 新增資料
		// test.insert(con, 1, "第一個");
		// test.insert(con, 2, "第二個");
		// test.insert(con, 3, "第三個");

		// 查詢顯示資料
		// System.out.println("新增二筆資料後狀況:");
		// test.selectAll(con);
		//
		// 修改資料
		// System.out.println("修改第一筆資料後狀況:");
		// test.update(con, 4, "這個值被改變了!");
		// // 查詢顯示資料
		// test.selectAll(con);
		// // 刪除資料
		// // System.out.println("刪除第一筆資料後狀況:");
		// // test.delete(con, 1);
		// // 查詢顯示資料
		// test.selectAll(con);
		//
		// // 刪除table
		// // test.dropTable(con);
		//
		// con.close();
		// Sqlite app = new Sqlite();
		// app.selectAll();
		// app.selectAll();

//	}

	public void updateSql(String fileName, String ckipResult, String sql)
			throws SQLException {
		// TODO Auto-generated method stub
		try {

			Connection conn = this.connect();

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// loop through the result set
			// while (rs.next()) {
			// System.out.println(rs.getString(columnName));
			// if (rs.getString(columnName) != null) {
			// return true;
			// }
			// }
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		// return false;

	}

}