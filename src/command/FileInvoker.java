package command;

import java.sql.SQLException;

public class FileInvoker {
	 
    public Command command;
     
    public FileInvoker(Command c){
        this.command=c;
    }
     
    public void execute() throws ClassNotFoundException, SQLException{
        this.command.execute();
    }
}