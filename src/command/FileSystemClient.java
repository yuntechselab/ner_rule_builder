package command;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.DatabaseMetaData;
public class FileSystemClient {
	 
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
    	
    	// register the driver 
        String sDriverName = "org.sqlite.JDBC";
        Class.forName(sDriverName);
 
        // now we set up a set of fairly basic string variables to use in the body of the code proper
        String sTempDb = "hello.db";
        String sJdbc = "jdbc:sqlite";
        String sDbUrl = sJdbc + ":" + sTempDb;
        // which will produce a legitimate Url for SqlLite JDBC :
        // jdbc:sqlite:hello.db
        int iTimeout = 30;
        String sMakeTable = "CREATE TABLE dummy (id numeric, response text)";
        String sMakeInsert = "INSERT INTO dummy VALUES(1,'Hello from the database')";
        String sMakeSelect = "SELECT response from dummy";
 

        // create a database connection
        Connection conn = DriverManager.getConnection(sDbUrl);
      
            Statement stmt = conn.createStatement();
 
                stmt.setQueryTimeout(iTimeout);
                stmt.executeUpdate( sMakeInsert );
         
       
    	
    	
        //Creating the receiver object
        FileSystemReceiver fs = FileSystemReceiverUtil.getUnderlyingFileSystem();
         
        //creating command and associating with receiver
        OpenFileCommand openFileCommand = new OpenFileCommand(fs);
         
        //Creating invoker and associating with Command
        FileInvoker file = new FileInvoker(openFileCommand);
         
        //perform action on invoker object
        file.execute();
         
        WriteFileCommand writeFileCommand = new WriteFileCommand(fs);
        file = new FileInvoker(writeFileCommand);
        file.execute();
         
        CloseFileCommand closeFileCommand = new CloseFileCommand(fs);
        file = new FileInvoker(closeFileCommand);
        file.execute();
    }
 
}