package command;

import java.sql.SQLException;

public interface FileSystemReceiver {
	 
    void openFile() throws SQLException;
    void writeFile();
    void closeFile();
	void registerDriver() throws ClassNotFoundException;
}