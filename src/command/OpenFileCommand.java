package command;

import java.sql.SQLException;

public class OpenFileCommand implements Command {
	 
    private FileSystemReceiver fileSystem;
     
    public OpenFileCommand(FileSystemReceiver fs){
        this.fileSystem=fs;
    }
    @Override
    public void execute() throws SQLException {
        //open command is forwarding request to openFile method
        this.fileSystem.openFile();
    }
 
}