package command;

public class RegisterDriverCommand implements Command {
	 
    private FileSystemReceiver fileSystem;
     
    public RegisterDriverCommand(FileSystemReceiver fs){
        this.fileSystem=fs;
    }
    @Override
    public void execute() throws ClassNotFoundException {
        this.fileSystem.registerDriver();
    }
 
}
