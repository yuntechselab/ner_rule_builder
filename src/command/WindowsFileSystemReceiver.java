package command;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class WindowsFileSystemReceiver implements FileSystemReceiver {

	@Override
	public void registerDriver() throws ClassNotFoundException {
		// register the driver
		String sDriverName = "org.sqlite.JDBC";
		Class.forName(sDriverName);

		// now we set up a set of fairly basic string variables to use in the
		// body of the code proper
		String sTempDb = "hello.db";
		String sJdbc = "jdbc:sqlite";
		String sDbUrl = sJdbc + ":" + sTempDb;
	}

	public void openFile() throws SQLException {
		System.out.println("Opening file in Windows OS");
		String sTempDb = "hello.db";
        String sJdbc = "jdbc:sqlite";
        String sDbUrl = sJdbc + ":" + sTempDb;
		  Connection conn = DriverManager.getConnection(sDbUrl);
	}

	@Override
	public void writeFile() {
		System.out.println("Writing file in Windows OS");
	}

	@Override
	public void closeFile() {
		System.out.println("Closing file in Windows OS");
	}

}