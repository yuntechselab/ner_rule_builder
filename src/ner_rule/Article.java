package ner_rule;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rule.Rule;

public class Article {
	List<String> posList = new ArrayList<String>();

	public List<String> getPosList() {
		return posList;
	}

	private Rule rule;

	public Article(Rule rule) {
		this.rule = rule;
	}

	public ArrayList<List> executeNerStrategy(Rule personRule)
			throws IOException {
		return personRule.nameEntityRecognize(this);
	}

	public void setPosList(List<String> posList) {
		this.posList = posList;
	}

	public List<String> getTermList() {
		return termList;
	}

	public void setTermList(List<String> termList) {
		this.termList = termList;
	}

	List<String> termList = new ArrayList<String>();
	ArrayList<List> allPosPersonResult = new ArrayList<List>();
	ArrayList<List> somePosPersonResult = new ArrayList<List>();
	String fileName = "";
	String content = "";
	String ckipResult = "";

	public void setCkipResult(String ckipResult) {
		this.ckipResult = ckipResult;
	};

	public String getContent() {
		return content;
	};

	public ArrayList<List> getAllPosPersonResult() {
		return allPosPersonResult;
	}

	public void setAllPosPersonResult(ArrayList<List> allPosPersonResult) {
		this.allPosPersonResult = allPosPersonResult;
	}

	public ArrayList<List> getSomePosPersonResult() {
		return somePosPersonResult;
	}

	public void setSomePosPersonResult(ArrayList<List> somePosPersonResult) {
		this.somePosPersonResult = somePosPersonResult;
	}

	public Article(List<String> posList, List<String> termList,
			String fileName, String content) {
		this.posList = posList;
		this.termList = termList;
		this.fileName = fileName;
		this.content = content;
	}

	public Article() {
	}

	public Article(String fileName) {
		// TODO Auto-generated constructor stub
		this.fileName = fileName;
	}

	public void keepPos() {
		String[] keepPos = { "Caa", "Na", "Nb", "Nc", "Ncd", "Nd", "Neu",
				"Nes", "Nequ","Nf", "VA", "VAC", "VB", "VC", "VCL", "VD", "VE",
				"VF", "VG", "VH", "VHC", "VI", "VJ", "VK", "VL", "V_2" };
		boolean flag = false;

		for (int i = 0; i < posList.size(); i++) {
			flag = false;
			for (int n = 0; n < keepPos.length; n++) {
				if (posList.get(i).equals(keepPos[n]) == true) {
					flag = true;
					break;
				}
			}

			if (flag == false) {
				posList.remove(i);
				termList.remove(i);
				i--;
			}
		}
		// System.out.println(posList.size());
		// System.out.println(termList.size());
	}

	public String getCkipResult(String ckipDirectory) throws IOException {
		System.out.println(content);
		if (FileTools.readFile(ckipDirectory + "/" + fileName) == null) {
			System.out.println("no ckip file");
			//Ckip ckip = new Ckip();
			ckipResult = Ckip.getCkipResult(content);
		} else {
			ckipResult = FileTools.readFile(ckipDirectory + "/" + fileName);
			System.out.println("yes ckip file");
		}
		return ckipResult;
	}

	public String getCkipResult() {
		
		return ckipResult;
	}

	public void segement1() {
		String rowToken = "\n----------------------------------------------------------------------------------------------------------------------------------\n";
		String rowToken2 = "----------------------------------------------------------------------------------------------------------------------------------";
		String splitHeadToken = "("; // Token
		String splitTailToken = ")"; // Token
		// System.out.println(ckipResult);
		for (String s : ckipResult.split("　")) {
			String term = "";
			String pos = "";
			if (s.equals("")) {
				continue;
			}

			s = s.replace(rowToken2, "");
			String checkToken = s.substring(0, s.length()).trim();
//			System.out.println("test" + checkToken);
			// if((checkToken.indexOf("<")) != -1){
			// continue;
			// }
			if (checkToken.equals("")) {
				continue;
			}
			if (checkToken.indexOf(splitHeadToken) != -1
					|| checkToken.indexOf(splitTailToken) != -1) {
				term = s.substring(0, s.indexOf(splitHeadToken)).trim();
				pos = s.substring(s.indexOf(splitHeadToken) + 1, s.length() - 1)
						.trim();
				if (pos.indexOf(splitTailToken) != -1) {
					pos = pos.replace(splitTailToken, "");
				}
			} else {
				term = s.substring(0, s.indexOf(splitHeadToken)).trim();
				pos = s.substring(s.indexOf(splitHeadToken) + 1, s.length() - 1)
						.trim();
			}
			posList.add(pos);
			termList.add(term);
		}

	}

	public void segement() {
		String rowToken = "\n----------------------------------------------------------------------------------------------------------------------------------\n";
		String rowToken2 = "----------------------------------------------------------------------------------------------------------------------------------";
		String splitHeadToken = "("; // ����Token
		String splitTailToken = ")"; // ����Token
		// System.out.println(ckipResult);
		System.out.println(ckipResult);
		for (String s : ckipResult.split("　")) {
			String term = "";
			String pos = "";

			s = s.replace(rowToken2, "");
			String checkToken = s.substring(0, s.length());
//			 System.out.println("checkToken: " + checkToken);
			 if (checkToken.indexOf(splitHeadToken) != -1){
					term = s.substring(0, s.indexOf(splitHeadToken)).trim();
					pos = s.substring(s.indexOf(splitHeadToken) + 1, s.length() - 1);

					posList.add(pos);
					termList.add(term);
					checkToken="";
					}
		}

	}

	static String RemovingCkip(String CKIPword) {

		try {
			CKIPword = CKIPword
					.replace(
							"----------------------------------------------------------------------------------------------------------------------------------",
							"");
			CKIPword = CKIPword.replace("�C(PERIODCATEGORY)", "");
			CKIPword = CKIPword.replace("\n", "");
			CKIPword = CKIPword.replace("�@�@", "");
			CKIPword = CKIPword.replace("�D(PERIODCATEGORY)", "");
			CKIPword = CKIPword.replace("�q(PARENTHESISCATEGORY)", "");
			CKIPword = CKIPword.replace("�E(PERIODCATEGORY)", "");

			CKIPword = CKIPword.replace("�A(COMMACATEGORY)", "");
			CKIPword = CKIPword.replace("�r(PARENTHESISCATEGORY)", "");
			CKIPword = CKIPword.replace("?(QUESTIONCATEGORY)", "");
			CKIPword = CKIPword.replace("�P(PERIODCATEGORY)", "");
			CKIPword = CKIPword.replace("�I(EXCLAMATIONCATEGORY)", "");
			CKIPword = CKIPword.replace("[(PARENTHESISCATEGORY)", "");
			CKIPword = CKIPword.replace("](PARENTHESISCATEGORY)", "");

			CKIPword = CKIPword.replace("�K(ETCCATEGORY)", "");
			CKIPword = CKIPword.replace("?(QUESTIONCATEGORY)", "");
			CKIPword = CKIPword.replace("-(FW)", "");
			CKIPword = CKIPword.replace("-", "");
			CKIPword = CKIPword.replace(";(SEMICOLONCATEGORY)", "");
			CKIPword = CKIPword.replace("\"(FW)", "");
			CKIPword = CKIPword.replace(":(COLONCATEGORY)", "");
			CKIPword = CKIPword.replace("(PARENTHESISCATEGORY)", "");
			CKIPword = CKIPword.replace("�H(QUESTIONCATEGORY)", "");
			CKIPword = CKIPword.replace("�e(PARENTHESISCATEGORY)", "");
			CKIPword = CKIPword.replace("�](PARENTHESISCATEGORY)", "");

			return CKIPword;
		} catch (Exception e) {
			System.err.println(e.toString());
			// System.exit(0);
		}
		return CKIPword;
	}

	public void setContent(String content) {
		// TODO Auto-generated method stub
		this.content = content;
	}

	public void setFileName(String fileName) {
		// TODO Auto-generated method stub
		this.fileName = fileName;
	}

}
