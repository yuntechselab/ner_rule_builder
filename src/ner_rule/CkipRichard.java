package ner_rule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class CkipRichard {

	Socket socket;

	public CkipRichard() {
		try {
			InetAddress ip = InetAddress.getByName("140.109.19.104");
			socket = new Socket(ip, 1501);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// CKIP main code
	public String getCkipResult(String content) {
		try {
			content = content.replaceAll("[<>]", "");
			//content = content.replace(">", "");

			PrintStream oos = new PrintStream(socket.getOutputStream());
			BufferedReader ois = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			oos.print("<?xml version=\"1.0\" ?>"
					+ "<wordsegmentation version=\"0.1\" charsetcode=\"UTF-8\">"
					+ "<option showcategory=\"1\" />"
					+ "<authentication username=\"selab1234\" password=\"selab1234\" />"
					+ "<text>" + content + "</text>" + "</wordsegmentation>");
			content = (String) ois.readLine();

			content = xmlreplace(content);

			return content;
		} catch (IOException e) {
			System.err.println("Error!!! :|");
			return "Ckip Error";
		}

	}

	// check processstatus code
	public String xmlreplace(String xmlcontent) {

		if (xmlcontent.indexOf("code=\"0\"") != -1) {
			xmlcontent = xmlcontent
					.replace(
							"<?xml version=\"1.0\" ?><wordsegmentation version=\"0.1\"><processstatus code=\"0\">Success</processstatus><result><sentence>",
							"");
			xmlcontent = xmlcontent.replace(
					"</sentence></result></wordsegmentation>", "");
			xmlcontent = xmlcontent.replace(
					"</sentence><sentence>", "");
		}

		else if (xmlcontent.indexOf("code=\"1\"") != -1) {
			xmlcontent = "Service internal error";
		}

		else if (xmlcontent.indexOf("code=\"2\"") != -1) {
			xmlcontent = "XML format error";
		}

		else {
			xmlcontent = "Authentication failed";
		}

		return xmlcontent;
	}

}