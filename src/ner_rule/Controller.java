package ner_rule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import rule.PersonRule;
import rule.PlaceRule;

public class Controller {
	private Model model;
	private View view;

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
	}

	public void mainFunction(String allPosQuestionDirectory,
			String somePosQuestionDirectory, String articleDirectory,
			String ckipDirectory) throws IOException {

		Directory directory = new Directory(articleDirectory);
		for (String fileName : directory.getDirectoryAllFileName()) {
			Article article = new Article();
			article.setFileName(fileName);
			article.setContent(FileTools.readFile(articleDirectory + "/"
					+ fileName));
			article.setCkipResult(article.getCkipResult(ckipDirectory)); // CKIP
			article.segement();
//			System.out.println(article.getPosList());
//			System.out.println(article.getTermList());

			article.setAllPosPersonResult(article
					.executeNerStrategy(new PersonRule()));
			// System.exit(0);
			article.keepPos();
			article.setSomePosPersonResult(article
					.executeNerStrategy(new PersonRule()));

			// save file to directory
			view.saveCkipResult(article.getCkipResult(), ckipDirectory + "/"
					+ fileName);
			view.saveAllPosPersonResult(article.getAllPosPersonResult(),
					allPosQuestionDirectory + "/" + fileName);
			view.saveSomePosPersonResult(article.getSomePosPersonResult(),
					somePosQuestionDirectory + "/" + fileName);
		}
	}

	public void compareAnswer(String answerDirectory, String questionDirectory,
			String missAndWrongDirectory, String resultDirectory)
			throws Exception {
		Answer answer = new Answer();
		answer.mainCompareAll(answerDirectory, questionDirectory);
		view.saveMissandWrong(answer.getMissAndWrong(), resultDirectory + "/"
				+ "missAndWrong.txt");
		WriteListToExcelFile.writeAnswerTermToFile("MET2_result/test.xls",
				answer.getTermResultArray());
	}


}
