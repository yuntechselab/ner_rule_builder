package ner_rule;

import java.io.*;
import java.util.ArrayList;

public class ReadFile {

	public ArrayList<String> read(String fileName, int number) {
		ArrayList<String> arraylist = new ArrayList<String>();

		// The name of the file to open.
		// This will reference one line at a time
		String line;
		try {
			// FileReader reads text files in the default encoding.

			// FileReader fileReader =
			// new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			// BufferedReader bufferedReader =
			// new BufferedReader(fileReader);

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(fileName),
							"UTF-8"));

			while ((line = bufferedReader.readLine()) != null) {
				// System.out.println(line);
				arraylist.add(line);
				// System.out.println(line);
			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
			// Or we could just do this:
			// ex.printStackTrace();
		}
		return arraylist;
	}

	public String read(String fileName) {

		// The name of the file to open.
		// This will reference one line at a time
		String line = null;
		String content = "";
		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				// System.out.println(line);
				content = content + line;
				// System.out.println(content);
			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
			// Or we could just do this:
			// ex.printStackTrace();
		}
		return content;
	}
}