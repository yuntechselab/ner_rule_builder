package ner_rule;

import java.io.IOException;
import java.util.ArrayList;

public class Replacement {
	public  static void replaceArticle(String directoryName) throws IOException {
		Directory directory = new Directory(directoryName);
		for (String fileName : directory.getDirectoryAllFileName()) {
			String a = FileTools.readFile(directoryName + "/" + fileName);
			if (a.contains("‧")) {
				System.out.println(a);
				a = a.replace('•', '‧');
				System.out.println(a);
			}
			FileTools.writeText(a, directoryName + "/" + fileName, "UTF-8",
					false);

		}
	}

	public static void replaceArticleArray(String directoryName)
			throws IOException {
		Directory directory = new Directory(directoryName);
		for (String fileName : directory.getDirectoryAllFileName()) {
			ArrayList<String> arrayList = FileTools.read(directoryName + "/"
					+ fileName);
			ArrayList<String> newArrayList = new ArrayList<String>();
			for (String string : arrayList) {
				if (string.contains("•")) {
					System.out.println(string);
					string = string.replace('•', '‧');
					System.out.println(string);
				}
				newArrayList.add(string);
			}
			FileTools.writeArrayText(newArrayList, directoryName + "/" + fileName,
					"UTF-8", false);
		}
	}
}
