package ner_rule;

import java.util.ArrayList;
import java.util.HashMap;

public class RuleCount {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> allPosQuestionArray = new ArrayList<String>();
		ArrayList<String> somePosQuestionArray = new ArrayList<String>();

		String allPosQuestionDirectory = "Yahoo_person_question_allPos";
		String somePosQuestionDirectory = "Yahoo_person_question_somePos";

		ArrayList<String> ruleArray = new ArrayList<String>();
		HashMap<String, Integer> ruleMap = new HashMap<String, Integer>();
		ruleArray = FileTools.read("personRule.txt");
		for (String rule : ruleArray) {
			ruleMap.put(rule, 0);
		}

//		Directory directory = new Directory(allPosQuestionDirectory);
//		for (String fileName : directory.getDirectoryAllFileName()) {
//			allPosQuestionArray = FileTools.read(allPosQuestionDirectory + "/"
//					+ fileName);
//			System.out.println(fileName);
//			// somePosQuestionArray = FileTools.read(questionDirectory + "/" +
//			// fileName);
//			compareRuleCount(allPosQuestionArray, ruleMap, ruleArray);
//		}
//		for (Object key : ruleMap.keySet()) {
//			System.out.println(key + " : " + ruleMap.get(key));
//		}
		
		Directory directory = new Directory(somePosQuestionDirectory);
		for (String fileName : directory.getDirectoryAllFileName()) {
		
			 somePosQuestionArray = FileTools.read(somePosQuestionDirectory + "/" +
			 fileName);
			compareRuleCount(somePosQuestionArray, ruleMap, ruleArray);
		}
		for (Object key : ruleMap.keySet()) {
			System.out.println(key + " : " + ruleMap.get(key));
		}
	}

	private static void compareRuleCount(ArrayList<String> allPosQuestionArray,
			HashMap<String, Integer> ruleMap, ArrayList<String> ruleArray) {
		// TODO Auto-generated method stub
		for (String question : allPosQuestionArray) {

			// 計算規則符合情況
			for (String rule : ruleArray) {
				int ruleIndex = question.indexOf(rule);
				if (ruleIndex != -1) {
					int ruleMapValue = ruleMap.get(rule) + 1;
					ruleMap.put(rule, ruleMapValue);
				}

			}
		}
	}
}
