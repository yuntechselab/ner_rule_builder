package ner_rule;

import java.util.ArrayList;
import java.util.List;

public class View {
	public void printStudentDetails(String studentName, String studentRollNo) {
		System.out.println("Student: ");
		System.out.println("Name: " + studentName);
		System.out.println("Roll No: " + studentRollNo);
	}

	public void saveCkipResult(String ckipResult, String filePath) {
		// TODO Auto-generated method stub
		FileTools.writeText(ckipResult, filePath, "UTF-8", false);
		System.out.println("saveCkipResult save success");
	}

	public void saveAllPosPersonResult(ArrayList<List> allPosPersonResult,
			String filePath) {
		// TODO Auto-generated method stub
		FileTools.writeText(allPosPersonResult, filePath, "UTF-8", false);
		System.out.println(filePath + " save success");
	}

	public void saveSomePosPersonResult(ArrayList<List> somePosPersonResult,
			String filePath) {
		// TODO Auto-generated method stub
		FileTools.writeText(somePosPersonResult, filePath, "UTF-8", false);
		System.out.println(filePath + "save success");
	}

	public void saveMissandWrong(ArrayList<List> missAndWrong, String filePath) {
		// TODO Auto-generated method stub
		FileTools.writeText(missAndWrong, filePath, "UTF-8", false);
		System.out.println(filePath + " save success");
	}

	
}
