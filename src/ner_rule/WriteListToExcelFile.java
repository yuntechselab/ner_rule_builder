package ner_rule;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteListToExcelFile {
	 public static void writeCountryListToFile(String fileName, List<String> countryList) throws Exception{
	        Workbook workbook = null;
	         
	        if(fileName.endsWith("xlsx")){
	            workbook = new XSSFWorkbook();
	        }else if(fileName.endsWith("xls")){
	            workbook = new HSSFWorkbook();
	        }else{
	            throw new Exception("invalid file name, should be xls or xlsx");
	        }
	         
	        Sheet sheet = workbook.createSheet("Countries");
	         
	        Iterator<String> iterator = countryList.iterator();
	         
	        int rowIndex = 0;
	        while(iterator.hasNext()){
	            String country = iterator.next();
	            Row row = sheet.createRow(rowIndex++);
	            Cell cell0 = row.createCell(0);
	            cell0.setCellValue(country);
	            Cell cell1 = row.createCell(1);
	            cell1.setCellValue("TEXT");
//	            Cell cell0 = row.createCell(0);
//	            cell0.setCellValue(country.getName());
//	            Cell cell1 = row.createCell(1);
//	            cell1.setCellValue(country.getShortCode());
	        }
	         
	        //lets write the excel data to file now
	        FileOutputStream fos = new FileOutputStream(fileName);
	        workbook.write(fos);
	        fos.close();
	        System.out.println(fileName + " written successfully");
	    }

	public static void writeCountryListToFile(String fileName,
			ArrayList<List> calculateResultArray) throws Exception {
		// TODO Auto-generated method stub
		 Workbook workbook = null;
         
	        if(fileName.endsWith("xlsx")){
	            workbook = new XSSFWorkbook();
	        }else if(fileName.endsWith("xls")){
	            workbook = new HSSFWorkbook();
	        }else{
	            throw new Exception("invalid file name, should be xls or xlsx");
	        }
	         
	        Sheet sheet = workbook.createSheet("Test");
	         
	        Iterator<List> iterator = calculateResultArray.iterator();
	        
	        Row header = sheet.createRow(0);
		    header.createCell(0).setCellValue("Article Number");
		    header.createCell(0).setCellValue("Answer Number");
		    header.createCell(1).setCellValue("Wrong Number");
		    header.createCell(2).setCellValue("Correct Number");
		    header.createCell(3).setCellValue("Lost Number");
		    header.createCell(4).setCellValue("Precision");
		    header.createCell(5).setCellValue("Recall");
		    
	        int rowIndex = 1;
	        while(iterator.hasNext()){
	            List list = iterator.next();
	            Row row = sheet.createRow(rowIndex++);
	            for(int i = 0;  i < list.size();i++){
	            	row.createCell(i).setCellValue((double) list.get(i));
//	            	Cell cell0 = row.createCell(0);
//		            cell0.setCellValue((double) list.get(0));

	            }
	            
	        }
	       
		     
//		    Row dataRow = sheet.createRow(1);
//		    dataRow.createCell(0).setCellValue(14500d);
//		    dataRow.createCell(1).setCellValue(9.25);
//		    dataRow.createCell(2).setCellValue(3d);
//		    dataRow.createCell(3).setCellFormula("A2*B2*C2");
//		    dataRow.createCell(4).setCellFormula("A2*B2*C2");
	        //lets write the excel data to file now
	        FileOutputStream fos = new FileOutputStream(fileName);
	        workbook.write(fos);
	        fos.close();
	        System.out.println(fileName + " written successfully");
	}

	public static void writeAnswerTermToFile(String fileName,
			ArrayList<AnswerTerm> termResultArray) throws Exception {
		// TODO Auto-generated method stub
		 Workbook workbook = null;
         
	        if(fileName.endsWith("xlsx")){
	            workbook = new XSSFWorkbook();
	        }else if(fileName.endsWith("xls")){
	            workbook = new HSSFWorkbook();
	        }else{
	            throw new Exception("invalid file name, should be xls or xlsx");
	        }
	         
	        Sheet sheet = workbook.createSheet("Test");
	         
	        Iterator<AnswerTerm> iterator = termResultArray.iterator();
	        
	        Row header = sheet.createRow(0);
		    header.createCell(0).setCellValue("Article Name");
		    header.createCell(1).setCellValue("Answer Number");
		    header.createCell(2).setCellValue("Wrong Number");
		    header.createCell(3).setCellValue("Correct Number");
		    header.createCell(4).setCellValue("Lost Number");
		    header.createCell(5).setCellValue("Precision");
		    header.createCell(6).setCellValue("Recall");
		    
	        int rowIndex = 1;
	        while(iterator.hasNext()){
	            AnswerTerm answerTerm = iterator.next();
	            Row row = sheet.createRow(rowIndex++);
	            row.createCell(0).setCellValue(answerTerm.getFileName());
	            row.createCell(1).setCellValue(answerTerm.getAnswerNumber());
	            row.createCell(2).setCellValue(answerTerm.getWrongNumber());
	            row.createCell(3).setCellValue(answerTerm.getCorrectNumber());
	            row.createCell(4).setCellValue(answerTerm.getLostNumber());
	            row.createCell(5).setCellValue(answerTerm.getPrecision());
	            row.createCell(6).setCellValue(answerTerm.getRecall());
	        }
	       
		     
	        //lets write the excel data to file now
	        FileOutputStream fos = new FileOutputStream(fileName);
	        workbook.write(fos);
	        fos.close();
	        System.out.println(fileName + " written successfully");
	}
	     
//	    public static void main(String args[]) throws Exception{
//	        List<String> list = ReadExcelFileToList.readExcelData("D://test.xls");
//	        WriteListToExcelFile.writeCountryListToFile("D://test1.xls", list);
//	    }
}
